from django.db import models

# Models to represent user
class Rating(models.Model):
    name = models.CharField(max_length=50)
    nilai_sate = models.IntegerField(default=0)
    nilai_warteg = models.IntegerField(default=0)
    nilai_ayamrica = models.IntegerField(default=0)
    nilai_ayambakar = models.IntegerField(default=0)
    nilai_ayampenyet = models.IntegerField(default=0)
    nilai_friedchicken = models.IntegerField(default=0)
    nilai_gadogado = models.IntegerField(default=0)
    nilai_miayam = models.IntegerField(default=0)
    nilai_soto = models.IntegerField(default=0)
    nilai_nasgor = models.IntegerField(default=0)
    nilai_yosinoya = models.IntegerField(default=0)

class Makanan(models.Model):
    nama = models.CharField(max_length=50)
    nama_penjual = models.CharField(max_length=50)
    range_harga = models.CharField(max_length=50)
    gambar = models.CharField(max_length=500, default="")


    


    #p = Rating(name=row['Nama'], nilai_sate=row['Sate'], nilai_warteg=row['Warteg'], nilai_ayamrica=row['Ayam Rica - rica'], nilai_ayambakar=row['Ayam bakar'], nilai_ayampenyet=row['Ayam penyet'], nilai_friedchicken=row['Fried chicken'], nilai_gadogado=row['Gado - Gado'], nilai_miayam=row['Mie Ayam'], nilai_soto=row['Soto'], nilai_nasgor=row['Nasi goreng'], nilai_yosinoya=row['Yoshinoya'])