from django.shortcuts import render, redirect
from django.core import serializers
from .models import Rating, Makanan
from scipy import spatial


response = {}
# Create your views here.
def index(request):
    html = 'index.html'
    foods = Makanan.objects.all()
    response = {'foods' : foods}
    return render(request, html, response)

def calculate_recommendation(request):
    # vector dimana si makanan blum di rate sama user
    html = 'result.html'
    undefined_pos = []
    user_vector = []
    final_recomendation = []
    nilai_sate = request.POST.get("rating-1")
    nilai_warteg = request.POST.get("rating-2")
    nilai_ayamrica = request.POST.get("rating-3")
    nilai_ayambakar = request.POST.get("rating-4")
    nilai_ayampenyet = request.POST.get("rating-5")
    nilai_friedchicken = request.POST.get("rating-6")
    nilai_gadogado = request.POST.get("rating-7")
    nilai_miayam = request.POST.get("rating-8")
    nilai_soto = request.POST.get("rating-9")
    nilai_nasgor = request.POST.get("rating-10")
    nilai_yosinoya = request.POST.get("rating-11")

    user_vector.append(nilai_sate)
    user_vector.append(nilai_warteg)
    user_vector.append(nilai_ayamrica)
    user_vector.append(nilai_ayambakar)
    user_vector.append(nilai_ayampenyet)
    user_vector.append(nilai_friedchicken)
    user_vector.append(nilai_gadogado)
    user_vector.append(nilai_miayam)
    user_vector.append(nilai_soto)
    user_vector.append(nilai_nasgor)
    user_vector.append(nilai_yosinoya)

    for i in range(len(user_vector)):
        if user_vector[i] == None:
            user_vector[i] = 0
            undefined_pos.append(i)
        else:
            user_vector[i] = int(user_vector[i])

    print("before",user_vector)
    db_ratings = get_rating()
    cos_sim_result = cos_sim(user_vector,db_ratings)

    predicted_rating = get_user_rating(db_ratings, cos_sim_result, user_vector)
    print("after", predicted_rating)
    #print(db_ratings)
    
    for i in range(11):
        if(len(final_recomendation) == 3):
            break
        else:
            current_max = max(predicted_rating)
            current_max_pos = predicted_rating.index(current_max)
            if (current_max_pos in undefined_pos):
                print("yes")
                final_recomendation.append(current_max_pos+1)
            predicted_rating[current_max_pos] = 0

    print("undef", undefined_pos)
    print(user_vector)
    
    response['foods'] = Makanan.objects.all()
    response['hello'] = final_recomendation
    print(response['hello'])

    return render(request, html, response)

    
def get_rating():
    rating_list = Rating.objects.all()
    all_rating = []
    for i in rating_list:
        current = []
        current.append(int(i.nilai_sate))
        current.append(int(i.nilai_warteg))
        current.append(int(i.nilai_ayamrica))
        current.append(int(i.nilai_ayambakar))
        current.append(int(i.nilai_ayampenyet))
        current.append(int(i.nilai_friedchicken))
        current.append(int(i.nilai_gadogado))
        current.append(int(i.nilai_miayam))
        current.append(int(i.nilai_soto))
        current.append(int(i.nilai_nasgor))
        current.append(int(i.nilai_yosinoya))
        all_rating.append(current)
    
    return all_rating


def cos_sim(user_vector, db_rating):
    results = []
    for i in db_rating:
        result = 1 - spatial.distance.cosine(i, user_vector)
        results.append(result)
    return results


#get the index of max sim
def get_user_rating(db_vector, sim_vector, user_vector):
    # get the 2 most similar user in database
    max_dict = {}
    for i in range(2):
        max_similarity = max(sim_vector)
        max_position = sim_vector.index(max_similarity)
        max_dict[max_position] = max_similarity
        sim_vector[max_position] = 0
    
    for i in range(len(user_vector)):
        if user_vector[i] == 0:
            predicted_rating = 0
            for key, value in max_dict.items():
                #print(value, db_vector[key][i])
                #print(db_vector[key])
                predicted_rating += (value * db_vector[key][i])
            #print(predicted_rating)
            user_vector[i] = predicted_rating /2
            
    return user_vector
            



    # maxpos = vector.index(max(vector))
    # return maxpos, max(vector)
