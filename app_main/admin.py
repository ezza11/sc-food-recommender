from django.contrib import admin
from app_main.models import Rating, Makanan

# Register your models here.
admin.site.register(Rating)
admin.site.register(Makanan)