$(document).ready(function(){

/**
 * Shuffles array in place. ES6 version
 * @param {Array} a items An array containing the items.
 * credit to: https://stackoverflow.com/questions/6274339/how-can-i-shuffle-an-array
 */
function shuffle(a) {
    for (let i = a.length - 1; i > 0; i--) {
        const j = Math.floor(Math.random() * (i + 1));
        [a[i], a[j]] = [a[j], a[i]];
    }
    return a;
}

var myArray = ['10','1','2','3','4','5','6','7','8','9'];
shuffle(myArray);

for (var i = 0; i < 10; i ++) {
	$('#'+i+'star5').prop('checked', false);
	$('#'+i+'star4').prop('checked', false);
	$('#'+i+'star3').prop('checked', false);
	$('#'+i+'star2').prop('checked', false);
	$('#'+i+'star1').prop('checked', false);
}

for (var i = 0; i < 5; i++) {
	var n = myArray[i];
	$('#item'+n).show();
}

})
